Source: rocalution
Section: devel
Homepage: https://github.com/ROCm/rocALUTION
Priority: optional
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/rocm-team/rocalution.git
Vcs-Browser: https://salsa.debian.org/rocm-team/rocalution
Maintainer: Debian ROCm Team <debian-ai@lists.debian.org>
Uploaders: Maxime Chambonnet <maxzor@maxzor.eu>,
           Cordell Bloor <cgmb@slerp.xyz>,
Build-Depends: debhelper-compat (= 13),
               cmake (>= 3.26.4-3),
               rocm-cmake,
               libomp-dev (>= 1.13.0),
               mpi-default-dev,
               libgtest-dev
Build-Depends-Indep: dh-sequence-sphinxdoc <!nodoc>,
               doxygen <!nodoc>,
               python3-breathe <!nodoc>,
               python3-sphinx <!nodoc>,
               python3-sphinx-rtd-theme <!nodoc>,
               libjs-jquery <!nodoc>,
               libjs-mathjax <!nodoc>,
               libjs-sphinxdoc <!nodoc>,
               libjs-underscore <!nodoc>
Rules-Requires-Root: no

Package: librocalution0
Section: libs
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends},
Description: ROCm library for iterative sparse solvers - library
 rocALUTION is a library that provides iterative sparse preconditioners and
 solvers. The rocALUTION project began as a port of PARALUTION to the AMD ROCm
 platform. This library supports an OpenMP backend for multi-core CPUs and an
 MPI backend for multi-node clusters.
 .
 rocALUTION provides a C++ API containing implementations of fixed-point
 iteration schemes such as Jacobi iteration and Gauss-Seidel; Krylov subspace
 methods such as the conjugate gradient method and the biconjugate gradient
 stabilized method; a mixed-precision defect correction scheme; a Chebyshev
 iteration scheme; as well as geometric and algebraic multigrid solvers. There
 are also a wide variety of sparse preconditioners, including several based on
 matrix splitting schemes, factorization schemes, and approximate inverses.
 .
 This package provides the AMD ROCm rocALUTION library.

Package: librocalution-dev
Section: libdevel
Architecture: any
Depends: librocalution0 (= ${binary:Version}),${misc:Depends}, ${shlibs:Depends},
Description: ROCm library for iterative sparse solvers - headers
 rocALUTION is a library that provides iterative sparse preconditioners and
 solvers. The rocALUTION project began as a port of PARALUTION to the AMD ROCm
 platform. This library supports an OpenMP backend for multi-core CPUs and an
 MPI backend for multi-node clusters.
 .
 rocALUTION provides a C++ API containing implementations of fixed-point
 iteration schemes such as Jacobi iteration and Gauss-Seidel; Krylov subspace
 methods such as the conjugate gradient method and the biconjugate gradient
 stabilized method; a mixed-precision defect correction scheme; a Chebyshev
 iteration scheme; as well as geometric and algebraic multigrid solvers. There
 are also a wide variety of sparse preconditioners, including several based on
 matrix splitting schemes, factorization schemes, and approximate inverses.
 .
 This package provides the AMD ROCm rocALUTION development headers.

Package: librocalution0-tests
Section: libdevel
Architecture: any
Depends: librocalution0 (= ${binary:Version}),${misc:Depends}, ${shlibs:Depends},
Description: ROCm library for iterative sparse solvers - tests
 rocALUTION is a library that provides iterative sparse preconditioners and
 solvers. The rocALUTION project began as a port of PARALUTION to the AMD ROCm
 platform. This library supports an OpenMP backend for multi-core CPUs and an
 MPI backend for multi-node clusters.
 .
 rocALUTION provides a C++ API containing implementations of fixed-point
 iteration schemes such as Jacobi iteration and Gauss-Seidel; Krylov subspace
 methods such as the conjugate gradient method and the biconjugate gradient
 stabilized method; a mixed-precision defect correction scheme; a Chebyshev
 iteration scheme; as well as geometric and algebraic multigrid solvers. There
 are also a wide variety of sparse preconditioners, including several based on
 matrix splitting schemes, factorization schemes, and approximate inverses.
 .
 This package provides the AMD ROCm rocALUTION test binaries used for verifying
 that the library is functioning correctly.

Package: librocalution-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Build-Profiles: <!nodoc>
Depends: ${misc:Depends},
         ${sphinxdoc:Depends},
         libjs-mathjax,
Description: ROCm library for iterative sparse solvers - documentation
 rocALUTION is a library that provides iterative sparse preconditioners and
 solvers. The rocALUTION project began as a port of PARALUTION to the AMD ROCm
 platform. This library supports an OpenMP backend for multi-core CPUs and an
 MPI backend for multi-node clusters.
 .
 rocALUTION provides a C++ API containing implementations of fixed-point
 iteration schemes such as Jacobi iteration and Gauss-Seidel; Krylov subspace
 methods such as the conjugate gradient method and the biconjugate gradient
 stabilized method; a mixed-precision defect correction scheme; a Chebyshev
 iteration scheme; as well as geometric and algebraic multigrid solvers. There
 are also a wide variety of sparse preconditioners, including several based on
 matrix splitting schemes, factorization schemes, and approximate inverses.
 .
 This package provides the AMD ROCm rocALUTION documentation.
