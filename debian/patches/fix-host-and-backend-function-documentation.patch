From: Cordell Bloor <cgmb@slerp.xyz>
Date: Mon, 12 Feb 2024 16:26:21 -0700
Subject: fix host and backend function documentation

The use of \ingroup seems to break the sphinx references in api.html.
I'm not sure why this is working upstream, but not in Debian.

Forwarded: not-needed
---
 src/base/backend_manager.hpp | 22 +++++++++++-----------
 src/utils/allocate_free.hpp  |  6 +++---
 src/utils/time_functions.hpp |  2 +-
 3 files changed, 15 insertions(+), 15 deletions(-)

diff --git a/src/base/backend_manager.hpp b/src/base/backend_manager.hpp
index eb6952b..8b5cf98 100644
--- a/src/base/backend_manager.hpp
+++ b/src/base/backend_manager.hpp
@@ -103,7 +103,7 @@ namespace rocalution
         HIP  = 1
     };
 
-    /** \ingroup backend_module
+    /**
   * \brief Initialize rocALUTION platform
   * \details
   * \p init_rocalution defines a backend descriptor with information about the hardware
@@ -146,7 +146,7 @@ namespace rocalution
     ROCALUTION_EXPORT
     int init_rocalution(int rank = -1, int dev_per_node = 1);
 
-    /** \ingroup backend_module
+    /**
   * \brief Shutdown rocALUTION platform
   * \details
   * \p stop_rocalution shuts down the rocALUTION platform.
@@ -154,7 +154,7 @@ namespace rocalution
     ROCALUTION_EXPORT
     int stop_rocalution(void);
 
-    /** \ingroup backend_module
+    /**
   * \brief Set the accelerator device
   * \details
   * \p set_device_rocalution lets the user select the accelerator device that is supposed
@@ -166,7 +166,7 @@ namespace rocalution
     ROCALUTION_EXPORT
     void set_device_rocalution(int dev);
 
-    /** \ingroup backend_module
+    /**
   * \brief Set number of OpenMP threads
   * \details
   * The number of threads which rocALUTION will use can be set with
@@ -196,7 +196,7 @@ namespace rocalution
     ROCALUTION_EXPORT
     void set_omp_threads_rocalution(int nthreads);
 
-    /** \ingroup backend_module
+    /**
   * \brief Enable/disable OpenMP host affinity
   * \details
   * \p set_omp_affinity_rocalution enables / disables OpenMP host affinity.
@@ -207,7 +207,7 @@ namespace rocalution
     ROCALUTION_EXPORT
     void set_omp_affinity_rocalution(bool affinity);
 
-    /** \ingroup backend_module
+    /**
   * \brief Set OpenMP threshold size
   * \details
   * Whenever you want to work on a small problem, you might observe that the OpenMP host
@@ -225,7 +225,7 @@ namespace rocalution
     ROCALUTION_EXPORT
     void set_omp_threshold_rocalution(int threshold);
 
-    /** \ingroup backend_module
+    /**
   * \brief Print info about rocALUTION
   * \details
   * \p info_rocalution prints information about the rocALUTION platform
@@ -233,7 +233,7 @@ namespace rocalution
     ROCALUTION_EXPORT
     void info_rocalution(void);
 
-    /** \ingroup backend_module
+    /**
   * \brief Print info about specific rocALUTION backend descriptor
   * \details
   * \p info_rocalution prints information about the rocALUTION platform of the specific
@@ -245,7 +245,7 @@ namespace rocalution
     ROCALUTION_EXPORT
     void info_rocalution(const struct Rocalution_Backend_Descriptor& backend_descriptor);
 
-    /** \ingroup backend_module
+    /**
   * \brief Disable/Enable the accelerator
   * \details
   * If you want to disable the accelerator (without re-compiling the code), you need to
@@ -289,7 +289,7 @@ namespace rocalution
         unsigned int                                matrix_format,
         int                                         blockdim = 1);
 
-    /** \ingroup backend_module
+    /**
   * \brief Sync rocALUTION
   * \details
   * \p _rocalution_sync blocks the host until all active asynchronous transfers are completed.
@@ -297,7 +297,7 @@ namespace rocalution
     ROCALUTION_EXPORT
     void _rocalution_sync(void);
 
-    /** \ingroup backend_module
+    /**
   * \brief get rocALUTION backend architecture
   * \details
   * \p get_arch_rocalution returns the name of rocalution's backend architecture
diff --git a/src/utils/allocate_free.hpp b/src/utils/allocate_free.hpp
index 08c8a6f..9116a99 100644
--- a/src/utils/allocate_free.hpp
+++ b/src/utils/allocate_free.hpp
@@ -29,7 +29,7 @@
 namespace rocalution
 {
 
-    /** \ingroup backend_module
+    /**
   * \brief Allocate buffer on the host
   * \details
   * \p allocate_host allocates a buffer on the host.
@@ -46,7 +46,7 @@ namespace rocalution
     template <typename DataType>
     ROCALUTION_EXPORT void allocate_host(int size, DataType** ptr);
 
-    /** \ingroup backend_module
+    /**
   * \brief Free buffer on the host
   * \details
   * \p free_host deallocates a buffer on the host. \p *ptr will be set to NULL after
@@ -62,7 +62,7 @@ namespace rocalution
     template <typename DataType>
     ROCALUTION_EXPORT void free_host(DataType** ptr);
 
-    /** \ingroup backend_module
+    /**
   * \brief Set a host buffer to zero
   * \details
   * \p set_to_zero_host sets a host buffer to zero.
diff --git a/src/utils/time_functions.hpp b/src/utils/time_functions.hpp
index aa33985..a7d36a7 100644
--- a/src/utils/time_functions.hpp
+++ b/src/utils/time_functions.hpp
@@ -29,7 +29,7 @@
 namespace rocalution
 {
 
-    /** \ingroup backend_module
+    /**
   * \brief Return current time in microseconds
   */
     ROCALUTION_EXPORT
